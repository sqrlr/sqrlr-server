package net.poundex.sqrlr.server.graphql;

import graphql.schema.GraphQLScalarType;
import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.ReadOnlyRepository;
import net.poundex.sqrlr.server.SqrlrObject;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.GenericTypeResolver;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.graphql.execution.RuntimeWiringConfigurer;

@Configuration
@RequiredArgsConstructor
class GraphQlConfiguration implements ApplicationListener<ContextRefreshedEvent> {
	
	private final BatchLoaderRegistry batchLoaderRegistry;
	
	@Bean
	public RuntimeWiringConfigurer runtimeWiringConfigurer() {
		GraphQLScalarType scalarType = new GraphQLScalarType.Builder()
				.name("Instant").coercing(new InstantScalarCoercing()).build();
		
		return wiringBuilder -> wiringBuilder.scalar(scalarType);
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		event.getApplicationContext()
				.getBeansOfType(ReadOnlyRepository.class)
				.values()
				.forEach(this::registerLoader);
	}
	
	private <T extends SqrlrObject> void registerLoader(ReadOnlyRepository<T> repo) {
		batchLoaderRegistry
				.forTypePair(String.class, getRepositoryType(repo))
				.registerMappedBatchLoader((ids, env) -> repo
						.findAllById(ids)
						.collectMap(SqrlrObject::getId, so -> so));
	}

	@SuppressWarnings("unchecked")
	private <T extends SqrlrObject> Class<T> getRepositoryType(ReadOnlyRepository<T> repository) {
		return (Class<T>) GenericTypeResolver.getTypeVariableMap(repository.getClass())
				.values()
				.stream()
				.findFirst()
				.orElseThrow(IllegalStateException::new);
	}
}
