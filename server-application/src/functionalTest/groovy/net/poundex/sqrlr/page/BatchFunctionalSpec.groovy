package net.poundex.sqrlr.page

import net.poundex.sqrlr.api.model.scan.BatchModel
import net.poundex.sqrlr.server.page.CollectionRepository
import net.poundex.sqrlr.server.page.PageCollection
import net.poundex.sqrlr.test.MongoUtils
import net.poundex.sqrlr.test.PublisherUtils
import net.poundex.testing.spock.functional.AbstractFunctionalSpec
import net.poundex.testing.spock.functional.GraphQl
import net.poundex.testing.spock.functional.GraphQlResponseHasErrors
import org.bson.types.ObjectId
import org.spockframework.spring.EnableSharedInjection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.graphql.execution.ErrorType
import org.springframework.test.annotation.DirtiesContext

import spock.lang.Shared
import spock.lang.Stepwise

@Stepwise
@DirtiesContext
@EnableSharedInjection
class BatchFunctionalSpec extends AbstractFunctionalSpec implements GraphQl, MongoUtils, PublisherUtils {

	@Autowired
	@Shared
	CollectionRepository collectionRepository
	
	@Autowired
	ReactiveMongoTemplate mongoTemplate

	@Shared
	PageCollection myCollection

	private static String lastSeenId

	private final static String GET_ALL_BATCHES_QUERY = """{
			batches {
				id
				collection {
					id
					name
				}
				volume
				created
			}
		}"""

	private final static String CREATE_NEW_BATCH_QUERY = '''
			mutation createBatch($collectionId: String, $volume: String) {
				createBatch(createBatchInput: { collectionId :$collectionId, volume: $volume }) {
					id
					collection {
						id
						name
					}
					volume
					created
				}
			}
		'''

	private final static String GET_BATCH_BY_ID_QUERY = '''
			query batch($batchId: String) {
				batch(batchId: $batchId) {
					id
					collection {
						id
						name
					}
					volume
					created
				}
			}
'''

	void setupSpec() {
		myCollection = block collectionRepository.create("some-collection")
	}

	void "Return an empty list of batches"() {
		when:
		List<BatchModel> r = queryList(BatchModel, GET_ALL_BATCHES_QUERY)

		then:
		r.size() == 0
	}

	void "Create new batches"() {
		when:
		BatchModel r = query(BatchModel, CREATE_NEW_BATCH_QUERY,
				collectionId: myCollection.id,
				volume: "some-volume")

		then:
		r.id() ==~ anObjectId
		with(r.collection()) {
			id() == myCollection.id
			name() == myCollection.name
		}
		r.volume() == "some-volume"
//		r.created() == Instant.now() TODO

		cleanup:
		lastSeenId = r.id()
	}

	void "Retrieve saved batch"() {
		when:
		List<BatchModel> r = queryList(BatchModel, GET_ALL_BATCHES_QUERY)

		then:
		r.size() == 1
		with(r.first()) {
			id() == lastSeenId
			with(collection()) {
				id() == myCollection.id
				name() == myCollection.name
			}
			volume() == "some-volume"
//			created() == Instant.now() TODO
		}
	}

	void "Error when creating batch with non-existent collection"(String id) {
		when:
		query(BatchModel, CREATE_NEW_BATCH_QUERY,
				collectionId: id,
				volume: "some-volume")

		then:
		GraphQlResponseHasErrors r = thrown()
		r.responseErrors.size() == 1
		r.responseErrors.first().getErrorType() == ErrorType.NOT_FOUND

		where:
		id << ["some-fake-collection-id", new ObjectId().toString()]
	}

	void "Error when creating batch with invalid values"() {
		when:
		query(BatchModel, CREATE_NEW_BATCH_QUERY,
				collection: null,
				volume: "")

		then:
		GraphQlResponseHasErrors r = thrown()
		r.responseErrors.size() == 1
		with(r.responseErrors.first()) {
			errorType == ErrorType.BAD_REQUEST
			extensions.error == "UNPROCESSABLE_ENTITY"
			with((List<Map<String, Object>>) extensions.validationErrors) {
				size() == 2
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "createBatch.createBatchInput.volume"
				}
				it.find {
					it.type == "FIELD_ERROR" &&
							it.path == "createBatch.createBatchInput.collectionId"
				}
			}
		}
	}

	void "Retrieve single batch by ID"() { 
		when:
		BatchModel r = query(BatchModel, GET_BATCH_BY_ID_QUERY, 
				batchId: lastSeenId)

		then:
		with(r) {
			id() == lastSeenId
			with(collection()) {
				id() == myCollection.id
				name() == myCollection.name
			}
			volume() == "some-volume"
		}
	}
	
	void "Retrieve batch when collection not found"() {
		given:
		assert (block mongoTemplate.removeById(
				myCollection.id, "pageCollection")).deletedCount == 1
		
		when:
		BatchModel r = query(BatchModel, GET_BATCH_BY_ID_QUERY,
				batchId: lastSeenId)

		then:
		with(r) {
			id() == lastSeenId
			volume() == "some-volume"
		}
	}
}
