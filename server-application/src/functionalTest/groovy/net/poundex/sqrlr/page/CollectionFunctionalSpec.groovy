package net.poundex.sqrlr.page

import net.poundex.sqrlr.api.model.page.CollectionModel
import net.poundex.sqrlr.test.MongoUtils
import net.poundex.testing.spock.functional.AbstractFunctionalSpec
import net.poundex.testing.spock.functional.GraphQl
import spock.lang.Stepwise

@Stepwise
class CollectionFunctionalSpec extends AbstractFunctionalSpec implements GraphQl, MongoUtils {
	
	private static String lastSeenId
	private final String GET_ALL_COLLECTIONS_QUERY = """{
			collections {
				id
				name
			}
		}"""

	void "Return an empty list of collections"() {
		when:
		List<CollectionModel> r = queryList(CollectionModel, GET_ALL_COLLECTIONS_QUERY)

		then:
		r.size() == 0
	}
	
	void "Create new collections"() {
		when:
		CollectionModel r = query(CollectionModel, '''
			mutation createCollection($name: String) {
				createCollection(name: $name) {
					id
					name
				}
			}
		''', name: "name-1")

		then:
		r.id() ==~ anObjectId
		r.name() == "name-1"
		
		cleanup:
		lastSeenId = r.id()
	}
	
	void "Retrieve saved collection"() {
		when:
		List<CollectionModel> r = queryList(CollectionModel, GET_ALL_COLLECTIONS_QUERY)

		then:
		r.size() == 1
		with(r.first()) {
			id() == lastSeenId
			name() == "name-1"
		}
	}
}
