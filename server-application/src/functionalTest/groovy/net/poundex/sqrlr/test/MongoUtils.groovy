package net.poundex.sqrlr.test

import java.util.regex.Pattern

trait MongoUtils {
	static final Pattern anObjectId = ~/[a-f0-9]{24}/
}