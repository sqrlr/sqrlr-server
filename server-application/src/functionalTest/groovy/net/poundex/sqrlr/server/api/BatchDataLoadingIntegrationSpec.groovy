package net.poundex.sqrlr.server.api

import net.poundex.sqrlr.server.page.CollectionRepository
import net.poundex.sqrlr.server.page.PageCollection
import net.poundex.sqrlr.server.scan.Batch
import net.poundex.sqrlr.server.scan.BatchRepository
import net.poundex.sqrlr.test.PublisherUtils
import net.poundex.testing.spock.functional.AbstractFunctionalSpec
import net.poundex.testing.spock.functional.GraphQl
import org.spockframework.spring.SpringBean
import reactor.core.publisher.Flux

import java.time.Instant

class BatchDataLoadingIntegrationSpec extends AbstractFunctionalSpec implements GraphQl, PublisherUtils {

	private static final PageCollection aCollection = new PageCollection("some-collection-id", "some-collection")
	private static final Batch aBatch = new Batch("some-batch-id", "some-collection-id", "some-volume", Instant.now())
	
	@SpringBean
	CollectionRepository collectionRepository = Mock()
	
	@SpringBean
	BatchRepository batchRepository = Mock()
	
	private final static String FETCH_MULTIPLE_QUERY = '''
		query q($batchId: String) {
			batches {
				id
				collection {
					id
					name
				}
				collection {
					id
					name
				}
			}
			batch(batchId: $batchId) {
				collection {
					id
					name
				}
			}
		}
'''

	void "Use batch loader to resolve entities by ID only once"() {
		when:
		query(FETCH_MULTIPLE_QUERY, batchId: aBatch.id)
		
		then:
		1 * batchRepository.findAll() >> Flux.just(aBatch)
		1 * collectionRepository.findAllById( [ aCollection.id ].toSet() ) >> Flux.just(aCollection)
		1 * batchRepository.findAllById( [ aBatch.id ].toSet() ) >> Flux.just(aBatch)
		0 * batchRepository._
		0 * collectionRepository._
	}
}
