package net.poundex.sqrlr.server.image

import net.poundex.sqrlr.api.model.image.ImageModel
import net.poundex.sqrlr.test.MongoUtils
import net.poundex.testing.spock.functional.AbstractFunctionalSpec
import net.poundex.testing.spock.functional.GraphQl
import net.poundex.testing.spock.functional.Json
import org.springframework.core.io.ClassPathResource
import spock.lang.Shared
import spock.lang.Stepwise

import java.util.zip.CRC32
import java.util.zip.CheckedInputStream

@Stepwise
class ImageFunctionalSpec extends AbstractFunctionalSpec implements Json, MongoUtils, GraphQl {
	
	@Shared
	private final ClassPathResource blankImage = 
			new ClassPathResource("image/util/blank.jpg")
	
	@Shared
	private final ClassPathResource blankImage2 =
			new ClassPathResource("image/util/blank2.jpg")

	@Shared
	private long blankImageChecksum = -1
	
	@Shared
	private long blankImageChecksum2 = -1

	private static String lastSeenId
	private static String lastSeenId2
	
	void setupSpec() {
		blankImageChecksum = getChecksum(blankImage.inputStream)
		blankImageChecksum2 = getChecksum(blankImage2.inputStream)
	}

	void "Upload a new image"() {
		when:
		ImageModel image = postFile(
				"/image",
				blankImage,
				ImageModel.class)
		String id = image.id()
		
		then:
		id ==~ anObjectId
		with(image.uri().split("[/.]")) {
			it[0] == ""
			it[1] == "image"
			it[2] == id
			it[3] == "jpg"
		}
		
		cleanup:
		lastSeenId = id
	}
	
	void "Upload a second different image"() {
		expect:
		ImageModel file = postFile(
				"/image",
				blankImage2,
				ImageModel.class)
		
		cleanup:
		lastSeenId2 = file.id()
	}
	
	void "Download an image"() {
		when:
		byte[] image = getRaw("/image/${lastSeenId}.jpg")
		
		then:
		getChecksum(image) == blankImageChecksum
		
		when:
		byte[] image2 = getRaw("/image/${lastSeenId2}.jpg")

		then:
		getChecksum(image2) == blankImageChecksum2
	}

	void "Retrieve images"() {
		when:
		List<ImageModel> r = queryList(ImageModel, '''
			query {
				images {
					id
					uri
					created
				}
			}
''')

		then:
		r.size() == 2
		with(r.find { it.id() == lastSeenId }) {
			uri() == "/image/${lastSeenId}.jpg"
//			created() == Instant.now() TODO
		}
		with(r.find { it.id() == lastSeenId2 }) {
			uri() == "/image/${lastSeenId2}.jpg"
//			created() == Instant.now() TODO
		}
	}
	
	private static long getChecksum(InputStream inputStream) {
		CheckedInputStream checkedInputStream =
				new CheckedInputStream(inputStream, new CRC32())
		byte[] buffer = new byte[256]
		while (checkedInputStream.read(buffer, 0, buffer.length) >= 0) {}
		return checkedInputStream.getChecksum().getValue()
	}
	
	private static long getChecksum(byte[] bytes) {
		CRC32 crc32 = new CRC32()
		crc32.update(bytes, 0, bytes.length)
		return crc32.getValue()
	}
}
