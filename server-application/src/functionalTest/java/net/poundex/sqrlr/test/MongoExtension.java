package net.poundex.sqrlr.test;

import com.mongodb.client.result.DeleteResult;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Mono;

import java.io.Serializable;

class MongoExtension {
	public static Mono<DeleteResult> removeById(ReactiveMongoTemplate self, Serializable id, String collectionName) {
		return self.remove(Query.query(
				Criteria.where("_id").is(id)), collectionName);
	}
}
