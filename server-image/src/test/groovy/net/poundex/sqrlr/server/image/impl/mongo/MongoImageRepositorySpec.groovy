package net.poundex.sqrlr.server.image.impl.mongo

import com.mongodb.client.gridfs.model.GridFSFile
import net.poundex.sqrlr.server.image.Image
import net.poundex.sqrlr.test.PublisherUtils
import org.bson.BsonObjectId
import org.bson.Document
import org.bson.types.ObjectId
import org.objenesis.ObjenesisStd
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.ReactiveGridFsOperations
import org.springframework.data.mongodb.gridfs.ReactiveGridFsResource
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant
import java.time.temporal.ChronoUnit

class MongoImageRepositorySpec extends Specification implements PublisherUtils {
	ReactiveGridFsOperations gridFs = Stub()
	
	@Subject
	MongoImageRepository repository = new MongoImageRepository(gridFs)
	
	void "Create new images"() {
		given:
		ObjectId someId = new ObjectId()
		Flux<DataBuffer> someData = Stub()
		gridFs.store(someData, _ as String, _ as Document) >> { Flux<DataBuffer> d, String name, Document doc ->
			return Mono.just(someId)
		}

		when:
		Image image = block repository.create(someData)
		
		then:
		image.id == someId.toString()
	}
	
	void "Retrieve existing images"() {
		given:
		ObjectId someId = new ObjectId()
		List<DataBuffer> someDataBuffers = [Stub(DataBuffer), Stub(DataBuffer)]
		Flux<DataBuffer> someData = Flux.fromIterable(someDataBuffers)
		GridFSFile aGridFsFile = aGridFsFile()
		gridFs.getResource(aGridFsFile) >> Mono.just(Stub(ReactiveGridFsResource){
			getDownloadStream() >> someData
		})
		gridFs.findOne({ Query q -> q.getQueryObject().get("_id") == someId.toString() }) >>
				Mono.just(aGridFsFile)

		when:
		List<DataBuffer> data = block repository.getImageData(someId.toString())
		
		then:
		data == someDataBuffers
	}


	void "Retrieve images"() {
		given:
		Instant now = Instant.now()
		ObjectId someId = ObjectId.get()
		GridFSFile gridFSFile = 
				new GridFSFile(new BsonObjectId(someId), "", 0, 0, new Date(), 
				new Document(Map.of("created", Date.from(now))))
		gridFs.find({ Query q -> q.getQueryObject().size() == 0 }) >> Flux.just(
				gridFSFile)

		when:
		List<Image> images = block repository.findAll().collectList()

		then:
		images.size() == 1
		with(images.first()) {
			id == someId.toString()
			created == now.truncatedTo(ChronoUnit.MILLIS)
		}
	}
	
	private static GridFSFile aGridFsFile() {
		return new ObjenesisStd().getInstantiatorOf(GridFSFile).newInstance()
	}
}
