package net.poundex.sqrlr.server.image.api

import net.poundex.sqrlr.api.model.image.ImageModel
import net.poundex.sqrlr.api.model.scan.BatchModel
import net.poundex.sqrlr.server.image.Image
import net.poundex.sqrlr.server.image.ImageRepository
import net.poundex.sqrlr.server.scan.Batch
import net.poundex.sqrlr.test.PublisherUtils
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.codec.multipart.FilePart
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class ImageControllerSpec extends Specification implements PublisherUtils {
	
	FilePart filePart = Stub()
	ImageRepository imageRepository = Stub()
	
	@Subject
	ImageController controller = new ImageController(imageRepository)
	
	void "Upload new images"() {
		given:
		Flux<DataBuffer> imageDataFlux = Stub()
		filePart.content() >> imageDataFlux
		Instant now = Instant.now()
		imageRepository.create(imageDataFlux) >> Mono.just(new Image("some-image-id", now))
		
		when:
		ImageModel image = block controller.uploadNewImage(Mono.just(filePart))
		
		then:
		with(image) {
			id() == "some-image-id"
			uri() == "/image/some-image-id.jpg"
			created() == now
		}
	}
	
	void "Download existing image"() {
		given:
		Flux<DataBuffer> imageDataFlux = Stub()
		imageRepository.getImageData("some-image-id") >> imageDataFlux

		when:
		Flux<DataBuffer> r = controller.getImage("some-image-id")

		then:
		r.is(imageDataFlux)
	}

	void "Retrieve images"() {
		given:
		Instant now = Instant.now()
		imageRepository.findAll() >> Flux.just(
				new Image("some-id", now))

		when:
		List<ImageModel> images = block controller.images().collectList()

		then:
		images.size() == 1
		with(images.first()) {
			id() == "some-id"
			created() == now
			uri() == "/image/some-id.jpg"
		}
	}
}
