package net.poundex.sqrlr.server.image.impl.mongo;

import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.image.Image;
import net.poundex.sqrlr.server.image.impl.AbstractImageRepository;
import org.bson.Document;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.ReactiveGridFsOperations;
import org.springframework.data.mongodb.gridfs.ReactiveGridFsResource;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Date;

@Service
@RequiredArgsConstructor
class MongoImageRepository extends AbstractImageRepository {
	private final ReactiveGridFsOperations grisFs;

	@Override
	public Flux<Image> findAll() {
		return grisFs.find(new Query()).map(f -> new Image(
				f.getObjectId().toString(), 
				f.getMetadata().get("created", Date.class).toInstant()));
	}

	@Override
	public Mono<Image> findById(String id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Flux<Image> findAllById(Iterable<String> ids) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected Mono<Image> doCreate(Flux<DataBuffer> dataBuffers) {
		Document document = new Document();
		document.put("created", Instant.now());
		return grisFs.store(dataBuffers, "filename", document)
				.map(id -> {
					document.put("id", id);
					return document;
				})
				.map(this::fromPersistent);
	}

	private Image fromPersistent(Document document) {
		return new Image(
				document.getObjectId("id").toString(), 
				document.get("created", Instant.class));
	}

	@Override
	public Flux<DataBuffer> getImageData(String imageId) {
		return grisFs.findOne(Query.query(Criteria.where("_id").is(imageId)))
				.flatMap(grisFs::getResource)
				.flatMapMany(ReactiveGridFsResource::getDownloadStream);    
	}
}
