package net.poundex.sqrlr.server.image.impl;

import net.poundex.sqrlr.server.image.Image;
import net.poundex.sqrlr.server.image.ImageRepository;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class AbstractImageRepository implements ImageRepository {
	@Override
	public Mono<Image> create(Flux<DataBuffer> dataBuffers) {
		return doCreate(dataBuffers);
	}

	protected abstract Mono<Image> doCreate(Flux<DataBuffer> dataBuffers);
}