package net.poundex.sqrlr.server.image.api;

import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.api.model.image.ImageModel;
import net.poundex.sqrlr.server.image.Image;
import net.poundex.sqrlr.server.image.ImageRepository;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
class ImageController {
	
	private final ImageRepository imageRepository;
	
	@PostMapping(value = "/image", 
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Mono<ImageModel> uploadNewImage(@RequestPart("image") Mono<FilePart> image) {
		return image.flatMap(fp -> imageRepository.create(fp.content()))
				.map(this::toModel);
	}

	@GetMapping(value = "/image/{imageId}.jpg",
		produces = MediaType.IMAGE_JPEG_VALUE)
	public Flux<DataBuffer> getImage(@PathVariable String imageId) {
		return imageRepository.getImageData(imageId);
	}

	@QueryMapping
	public Flux<ImageModel> images() {
		return imageRepository.findAll().map(this::toModel);
	}

	private ImageModel toModel(Image image) {
		return new ImageModel(
				image.getId(), 
				String.format("/image/%s.jpg", image.getId()), 
				image.getCreated());
	}
}
