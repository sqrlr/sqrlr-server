package net.poundex.sqrlr.server.page;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.SqrlrObject;

@RequiredArgsConstructor
@Data
@Builder
public class PageCollection implements SqrlrObject {
	private final String id;
	private final String name;
}
