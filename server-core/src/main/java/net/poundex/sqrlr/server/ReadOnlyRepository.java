package net.poundex.sqrlr.server;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReadOnlyRepository<T extends SqrlrObject> {
	Flux<T> findAll();

	Mono<T> findById(String id);
	
	Flux<T> findAllById(Iterable<String> ids);
}
