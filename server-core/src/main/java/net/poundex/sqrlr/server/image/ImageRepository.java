package net.poundex.sqrlr.server.image;

import net.poundex.sqrlr.server.ReadOnlyRepository;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface ImageRepository extends ReadOnlyRepository<Image> {
	Mono<Image> create(Flux<DataBuffer> dataBuffers);

	Flux<DataBuffer> getImageData(String imageId);
}
