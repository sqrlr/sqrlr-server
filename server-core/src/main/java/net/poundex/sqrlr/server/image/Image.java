package net.poundex.sqrlr.server.image;

import lombok.Data;
import net.poundex.sqrlr.server.SqrlrObject;

import java.time.Instant;

@Data
public class Image implements SqrlrObject {
	private final String id;
	private final Instant created;
}
