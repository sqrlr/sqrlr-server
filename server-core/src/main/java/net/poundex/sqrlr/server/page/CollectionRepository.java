package net.poundex.sqrlr.server.page;

import net.poundex.sqrlr.server.ReadOnlyRepository;
import reactor.core.publisher.Mono;

public interface CollectionRepository extends ReadOnlyRepository<PageCollection> {
	Mono<PageCollection> create(String name);
}
