package net.poundex.sqrlr.server.scan;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.SqrlrObject;

import java.time.Instant;

@RequiredArgsConstructor
@Data
@Builder
public class Batch implements SqrlrObject {
	private final String id;
	private final String collection;
	private final String volume;
	private final Instant created;
}
