package net.poundex.sqrlr.server.scan;

import net.poundex.sqrlr.server.ReadOnlyRepository;
import reactor.core.publisher.Mono;

public interface BatchRepository extends ReadOnlyRepository<Batch> {
	Mono<Batch> create(String collectionId, String volume);
}
