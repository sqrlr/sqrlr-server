package net.poundex.sqrlr.server.page.impl.mongo

import net.poundex.sqrlr.server.page.PageCollection
import net.poundex.sqrlr.test.PublisherUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class CollectionMongoRepositorySpec extends Specification implements PublisherUtils {
	
	CollectionMongoDao collectionMongoDao = Stub()
	
	@Subject
	CollectionMongoRepository repository = new CollectionMongoRepository(collectionMongoDao)
	
	void "Create new collection"() {
		given:
		ObjectId someId = ObjectId.get()
		collectionMongoDao.save(_ as MongoPageCollection) >> { MongoPageCollection mpc ->
			return Mono.just(MongoPageCollection.builder().id(someId).name(mpc.getName()).build())
		}
		
		when:
		PageCollection collection = repository.create("some-name").block()
		
		then:
		with(collection) {
			id == someId.toString()
			name == "some-name"
		}
	}
	
	void "Retrieve collections"() {
		given:
		ObjectId someId = ObjectId.get()
		collectionMongoDao.findAll() >> Flux.just(
				MongoPageCollection.builder()
						.id(someId)
						.name("some-name")
						.build())

		when:
		List<PageCollection> collections = repository.findAll().collectList().block()

		then:
		collections.size() == 1
		with(collections.first()) {
			id == someId.toString()
			name == "some-name"
		}
	}
	
	void "Fetch collection by ID"() {
		given:
		ObjectId someId = ObjectId.get()
		collectionMongoDao.findById(someId) >> Mono.just(
				MongoPageCollection.builder()
						.id(someId)
						.name("some-name")
						.build())

		when:
		PageCollection collection = block repository.findById(someId.toString())

		then:
		with(collection) {
			id == someId.toString()
			name == "some-name"
		}
	}
	
	void "Return empty when ID is invalid"() {
		expect:
		repository.findById("some-fake-id") == Mono.empty()
	}
}
