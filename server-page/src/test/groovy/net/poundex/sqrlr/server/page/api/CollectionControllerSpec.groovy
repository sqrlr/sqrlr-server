package net.poundex.sqrlr.server.page.api

import net.poundex.sqrlr.api.model.page.CollectionModel
import net.poundex.sqrlr.server.page.CollectionRepository
import net.poundex.sqrlr.server.page.PageCollection
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class CollectionControllerSpec extends Specification {
	
	CollectionRepository collectionRepository = Stub()
	
	@Subject
	CollectionController collectionController = new CollectionController(collectionRepository)
	
	void "Create new collection"() {
		given:
		collectionRepository.create(_ as String) >> { String str -> 
			Mono.just(PageCollection.builder().id("some-id").name(str).build())
		}
		
		when:
		CollectionModel collection = collectionController.createCollection("some-name").block()
		
		then:
		with(collection) {
			id() == "some-id"
			name() == "some-name"
		}
	}
	
	void "Retrieve collections"() {
		given:
		collectionRepository.findAll() >> Flux.just(
				PageCollection.builder()
						.id("some-id")
						.name("some-name")
						.build())

		when:
		List<CollectionModel> collections = collectionController.collections().collectList().block()

		then:
		collections.size() == 1
		with(collections.first()) {
			id() == "some-id"
			name() == "some-name"
		}
	}
}
