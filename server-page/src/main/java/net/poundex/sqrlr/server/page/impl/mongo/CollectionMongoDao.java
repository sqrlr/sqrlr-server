package net.poundex.sqrlr.server.page.impl.mongo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface CollectionMongoDao extends ReactiveMongoRepository<MongoPageCollection, ObjectId> {
	
}
