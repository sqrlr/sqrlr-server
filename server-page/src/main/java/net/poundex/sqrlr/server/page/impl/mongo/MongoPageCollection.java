package net.poundex.sqrlr.server.page.impl.mongo;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("pageCollection")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class MongoPageCollection {
	@Id
	private ObjectId id;
	
	private String name;
}
