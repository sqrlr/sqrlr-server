package net.poundex.sqrlr.server.page.impl.mongo;

import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.page.CollectionRepository;
import net.poundex.sqrlr.server.page.PageCollection;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
class CollectionMongoRepository implements CollectionRepository {
	
	private final CollectionMongoDao collectionDao;
	
	@Override
	public Mono<PageCollection> create(String name) {
		return collectionDao.save(toPersistent(name))
				.map(this::fromPersistent); 
	}

	@Override
	public Flux<PageCollection> findAll() {
		return collectionDao.findAll().map(this::fromPersistent);
	}

	@Override
	public Mono<PageCollection> findById(String id) {
		return Try.of(() -> new ObjectId(id))
				.map(oid -> collectionDao
						.findById(oid)
						.map(this::fromPersistent))
				.recover(IllegalArgumentException.class, Mono.empty())
				.get();
	}

	@Override
	public Flux<PageCollection> findAllById(Iterable<String> ids) {
		Set<ObjectId> validIds = Stream.ofAll(ids)
				.map(id -> Try.of(() -> new ObjectId(id)))
				.filter(Try::isSuccess)
				.map(Try::get)
				.toSet();
		return collectionDao.findAllById(validIds).map(this::fromPersistent);
	}

	private MongoPageCollection toPersistent(String name) {
		return MongoPageCollection.builder()
				.name(name)
				.build();
	}

	private PageCollection fromPersistent(MongoPageCollection mongoPageCollection) {
		return PageCollection.builder()
				.id(mongoPageCollection.getId().toString())
				.name(mongoPageCollection.getName())
				.build();
	}
}
