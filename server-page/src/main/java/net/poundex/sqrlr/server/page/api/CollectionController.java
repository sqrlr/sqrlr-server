package net.poundex.sqrlr.server.page.api;

import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.api.model.page.CollectionModel;
import net.poundex.sqrlr.server.page.CollectionRepository;
import net.poundex.sqrlr.server.page.PageCollection;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequiredArgsConstructor
class CollectionController {

	private final CollectionRepository collectionRepository;
	
	@QueryMapping
	public Flux<CollectionModel> collections() {
		return collectionRepository.findAll().map(this::toModel);
	}

	@MutationMapping
	public Mono<CollectionModel> createCollection(@Argument String name) {
		return collectionRepository.create(name).map(this::toModel);
	}

	private CollectionModel toModel(PageCollection pc) {
		return new CollectionModel(pc.getId(), pc.getName());
	}
}
