package net.poundex.sqrlr.server.scan.impl.mongo;

import io.vavr.collection.Set;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import net.poundex.sqrlr.server.page.CollectionRepository;
import net.poundex.sqrlr.server.page.PageCollection;
import net.poundex.sqrlr.server.scan.Batch;
import net.poundex.sqrlr.server.scan.impl.AbstractBatchRepository;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Service
class BatchMongoRepository extends AbstractBatchRepository {

	private final BatchMongoDao batchDao;

	public BatchMongoRepository(BatchMongoDao batchDao, CollectionRepository collectionRepository) {
		super(collectionRepository);
		this.batchDao = batchDao;
	}

	@Override
	protected Mono<Batch> doCreate(PageCollection collection, String volume) {
		return batchDao.save(toPersistent(collection.getId(), volume, Instant.now()))
				.map(this::fromPersistent);
	}

	private MongoBatch toPersistent(String collection, String volume, Instant created) {
		return MongoBatch.builder()
				.collection(collection)
				.volume(volume)
				.created(created)
				.build();
	}

	private Batch fromPersistent(MongoBatch mongoBatch) {
		return Batch.builder()
				.id(mongoBatch.getId().toString())
				.collection(mongoBatch.getCollection())
				.volume(mongoBatch.getVolume())
				.created(mongoBatch.getCreated())
				.build();
	}

	@Override
	protected Flux<Batch> doFindAll() {
		return batchDao.findAll().map(this::fromPersistent);
	}

	@Override
	protected Mono<Batch> doFindById(String id) {
		return Try.of(() -> new ObjectId(id))
				.map(oid -> batchDao
						.findById(oid)
						.map(this::fromPersistent))
				.recover(IllegalArgumentException.class, Mono.empty())
				.get();
	}

	@Override
	protected Flux<Batch> doFindAllById(Iterable<String> ids) {
		Set<ObjectId> validIds = Stream.ofAll(ids)
				.map(id -> Try.of(() -> new ObjectId(id)))
				.filter(Try::isSuccess)
				.map(Try::get)
				.toSet();
		return batchDao.findAllById(validIds).map(this::fromPersistent);
	}
}
