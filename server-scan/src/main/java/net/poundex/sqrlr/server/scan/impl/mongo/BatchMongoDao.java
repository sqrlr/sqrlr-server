package net.poundex.sqrlr.server.scan.impl.mongo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

interface BatchMongoDao extends ReactiveMongoRepository<MongoBatch, ObjectId> {
}
