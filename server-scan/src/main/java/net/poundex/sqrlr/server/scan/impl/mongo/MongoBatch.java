package net.poundex.sqrlr.server.scan.impl.mongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document("batch")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class MongoBatch {
	@Id
	private ObjectId id;
	
	private String collection;
	private String volume;
	private Instant created;
}
