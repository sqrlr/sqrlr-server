package net.poundex.sqrlr.server.scan.impl;

import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.server.exception.NotFoundException;
import net.poundex.sqrlr.server.page.CollectionRepository;
import net.poundex.sqrlr.server.page.PageCollection;
import net.poundex.sqrlr.server.scan.Batch;
import net.poundex.sqrlr.server.scan.BatchRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public abstract class AbstractBatchRepository implements BatchRepository {
	
	private final CollectionRepository collectionRepository;
	
	@Override
	public Mono<Batch> create(String collectionId, String volume) {
		return collectionRepository
				.findById(collectionId)
				.switchIfEmpty(Mono.error(new NotFoundException(PageCollection.class, collectionId)))
				.flatMap(c -> doCreate(c, volume));
	}

	protected abstract Mono<Batch> doCreate(PageCollection collection, String volume);

	@Override
	public Flux<Batch> findAll() {
		return doFindAll();
	}

	protected abstract Flux<Batch> doFindAll();

	@Override
	public Mono<Batch> findById(String id) {
		return doFindById(id);
	}

	protected abstract Mono<Batch> doFindById(String id);

	@Override
	public Flux<Batch> findAllById(Iterable<String> ids) {
		return doFindAllById(ids);
	}

	protected abstract Flux<Batch> doFindAllById(Iterable<String> ids);
}
