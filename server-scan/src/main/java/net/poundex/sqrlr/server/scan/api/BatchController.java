package net.poundex.sqrlr.server.scan.api;

import graphql.schema.DataFetchingFieldSelectionSet;
import lombok.RequiredArgsConstructor;
import net.poundex.sqrlr.api.model.page.CollectionModel;
import net.poundex.sqrlr.api.model.scan.BatchModel;
import net.poundex.sqrlr.api.model.scan.CreateBatchInput;
import net.poundex.sqrlr.server.api.BatchLoadingHelper;
import net.poundex.sqrlr.server.page.PageCollection;
import net.poundex.sqrlr.server.scan.Batch;
import net.poundex.sqrlr.server.scan.BatchRepository;
import org.dataloader.DataLoader;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
class BatchController {

	private final BatchRepository batchRepository;
	private final BatchLoadingHelper batchLoadingHelper;

	@QueryMapping
	public Flux<BatchModel> batches() {
		return batchRepository.findAll().map(this::toModel);
	}

	@MutationMapping
	public Mono<BatchModel> createBatch(@Argument @Valid CreateBatchInput createBatchInput) {
		return batchRepository.create(createBatchInput.collectionId(), createBatchInput.volume()).map(this::toModel);
	}
	
	@SchemaMapping(typeName = "Batch", field = "collection")
	public Mono<CollectionModel> collection(
			BatchModel batchModel, 
			DataFetchingFieldSelectionSet fieldSelectionSet, 
			DataLoader<String, PageCollection> loader) {
		
		return batchLoadingHelper.loadIfNecessary(
						batchModel.collection().id(), fieldSelectionSet, loader)
				.andMap(c -> new CollectionModel(c.getId(), c.getName()))
				.orElse(() -> new CollectionModel(batchModel.collection().id(), null));
	}

	@QueryMapping
	public Mono<BatchModel> batch(@Argument String batchId, DataFetchingFieldSelectionSet fieldSelectionSet, DataLoader<String, Batch> loader) {
		return batchLoadingHelper.loadIfNecessary(batchId, fieldSelectionSet, loader)
				.get().orElseGet(Mono::empty)
				.map(this::toModel);
	}

	private BatchModel toModel(Batch batch) {
		return new BatchModel(batch.getId(), new CollectionModel(batch.getCollection(), null), 
				batch.getVolume(), batch.getCreated());
	}
}
