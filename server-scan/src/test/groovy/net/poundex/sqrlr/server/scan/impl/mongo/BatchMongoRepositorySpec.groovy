package net.poundex.sqrlr.server.scan.impl.mongo

import net.poundex.sqrlr.server.exception.NotFoundException
import net.poundex.sqrlr.server.page.CollectionRepository
import net.poundex.sqrlr.server.page.PageCollection
import net.poundex.sqrlr.server.scan.Batch
import net.poundex.sqrlr.test.PublisherUtils
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant

class BatchMongoRepositorySpec extends Specification implements PublisherUtils {
	
	private final static Instant now = Instant.now()
	
	BatchMongoDao batchMongoDao = Stub()
	CollectionRepository collectionRepository = Stub() {
		findById("some-collection-id") >> Mono.just(new PageCollection("some-collection-id", "some-collection"))
		findById(_ as String) >> Mono.empty()
	}
	
	@Subject
	BatchMongoRepository repository = new BatchMongoRepository(batchMongoDao, collectionRepository)
	
	void "Create new batch"() {
		given:
		ObjectId someId = ObjectId.get()
		batchMongoDao.save(_ as MongoBatch) >> { MongoBatch mpc ->
			return Mono.just(MongoBatch.builder()
					.id(someId)
					.collection("some-collection-id")
					.volume("some-volume")
					.created(now)
					.build())
		}
		
		when:
		Batch batch = block repository
				.create("some-collection-id", "some-volume")
		
		then:
		with(batch) {
			id == someId.toString()
			collection == "some-collection-id"
			volume == "some-volume"
			created == now
		}   
	}
	
	void "Retrieve batches"() {
		given:
		ObjectId someId = ObjectId.get()
		batchMongoDao.findAll() >> Flux.just(
				MongoBatch.builder()
						.id(someId)
						.collection("some-collection-id")
						.volume("some-volume")
						.created(now)
						.build())

		when:
		List<Batch> batches = block repository.findAll().collectList()

		then:
		batches.size() == 1
		with(batches.first()) {
			id == someId.toString()
			collection == "some-collection-id"
			volume == "some-volume"
			created == now
		}
	}
	
	void "Throw not found if trying to use a collection which doesn't exist"() {
		when:
		block repository.create("some-fake-collection-id", "some-volume")

		then:
		NotFoundException nfex = thrown()
		nfex.type == "PageCollection"
		nfex.id == "some-fake-collection-id"
	}
	
	void "Retrieve single batch by ID"() {
		given:
		ObjectId someId = ObjectId.get()
		batchMongoDao.findById(someId) >> Mono.just(
				MongoBatch.builder()
						.id(someId)
						.collection("some-collection-id")
						.volume("some-volume")
						.created(now)
						.build())

		when:
		Batch batches = block repository.findById(someId.toString())

		then:
		with(batches) {
			id == someId.toString()
			collection == "some-collection-id"
			volume == "some-volume"
			created == now
		}
	}
	
	void "Retrieve batches by ID"() {
		given:
		ObjectId someId = ObjectId.get()
		batchMongoDao.findAllById({ it.intersect([someId]).size() == 1 }) >> Flux.just(
				MongoBatch.builder()
						.id(someId)
						.collection("some-collection-id")
						.volume("some-volume")
						.created(now)
						.build())

		when:
		List<Batch> batches = block repository.findAllById( [ someId.toString() ] ).collectList()

		then:
		batches.size() == 1
		with(batches.first()) {
			id == someId.toString()
			collection == "some-collection-id"
			volume == "some-volume"
			created == now
		}
	}
}
