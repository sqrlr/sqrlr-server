package net.poundex.sqrlr.server.scan.api

import net.poundex.sqrlr.api.model.page.CollectionModel
import net.poundex.sqrlr.api.model.scan.BatchModel
import net.poundex.sqrlr.api.model.scan.CreateBatchInput
import net.poundex.sqrlr.server.api.BatchLoadingHelper
import net.poundex.sqrlr.server.page.PageCollection
import net.poundex.sqrlr.server.scan.Batch
import net.poundex.sqrlr.server.scan.BatchRepository
import net.poundex.sqrlr.test.BatchLoadingUtils
import net.poundex.sqrlr.test.PublisherUtils
import org.dataloader.DataLoader
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant
import java.util.concurrent.CompletableFuture

class BatchControllerSpec extends Specification implements PublisherUtils, BatchLoadingUtils {
	private final static Instant now = Instant.now()

	BatchRepository batchRepository = Stub()
	DataLoader<String, PageCollection> collectionLoader = Mock()
	DataLoader<String, Batch> batchLoader = Mock()
	
	@Subject
	BatchController batchController = new BatchController(batchRepository, new BatchLoadingHelper())
	
	void "Create new batch"() {
		given:
		batchRepository.create(_ as String, _ as String) >> { String collection, String volume ->
			Mono.just(Batch.builder()
					.id("some-id")
					.collection(collection)
					.volume(volume)
					.created(now)
					.build())
		}

		when:
		BatchModel batch = block batchController
				.createBatch(new CreateBatchInput("some-collection-id", "some-volume"))

		then:
		with(batch) {
			id() == "some-id"
			collection().id() == "some-collection-id"
			volume() == "some-volume"
			created() == now
		}
	}

	void "Retrieve batches"() {
		given:
		batchRepository.findAll() >> Flux.just(Batch.builder()
				.id("some-id")
				.collection("some-collection-id")
				.volume("some-volume")
				.created(now)
				.build())

		when:
		List<BatchModel> batches = block batchController.batches().collectList()

		then:
		batches.size() == 1
		with(batches.first()) {
			id() == "some-id"
			collection().id() == "some-collection-id"
			volume() == "some-volume"
			created() == now
		}
	}
	
	void "Return collections for batches"() {
		given:
		BatchModel aBatch = new BatchModel("some-id", 
				new CollectionModel("some-collection-id", null), "some-volume", Instant.now())
		
		
		when:
		CollectionModel collection = block batchController.collection(aBatch, fields("id", "name"), collectionLoader)
		
		then:
		1 * collectionLoader.load("some-collection-id") >>
				CompletableFuture.completedFuture(new PageCollection("some-collection-id", "some-collection"))
		
		and:
		with(collection) {
			id() == "some-collection-id"
			name() == "some-collection"
		}
	}

	void "Do not fetch collection if not requested"() {
		given:
		BatchModel aBatch = new BatchModel("some-id",
				new CollectionModel("some-collection-id", null), 
				"some-volume", Instant.now())
		
		when:
		CollectionModel collection = block batchController
				.collection(aBatch, justId(), collectionLoader)

		then:
		0 * collectionLoader._

		and:
		with(collection) {
			id() == "some-collection-id"
			name() == null
		}
	}
	
	void "Retrieve single batch by ID"() {
		given:
		batchLoader.load("some-id") >> CompletableFuture.completedFuture(Batch.builder()
				.id("some-id")
				.collection("some-collection-id")
				.volume("some-volume")
				.created(now)
				.build())

		when:
		BatchModel batches = block batchController
				.batch("some-id", fields("created"), batchLoader)

		then:
		with(batches) {
			id() == "some-id"
			collection().id() == "some-collection-id"
			volume() == "some-volume"
			created() == now
		}
	}
}
